# Changelog

## v1.0.0 (2024-07-04)

* Initial commit with StreamedResponse response and formatters for:
  * Copying the entire stream to the client
  * Applying the Range: header with a single range, respecting If-Range
  * Apply the Range: header with multiple ranges, respecting If-Range
