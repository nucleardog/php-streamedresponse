# Streamed Response

Extends Symfony's StreamedResponse to support serving `Range:` and other
requests.

## Usage

Within Symfony, Laravel, or any other framework relying on Symfony's
HttpFoundation component, you may return the `StreamedResponse` from
this package as a response and it will automatically handle serving
partial content in response to requests including a `Range:` header.

For example:

```php
<?php
class Controller
{
	public function getFile()
	{
		$fileStream = \Nucleardog\Streams\ReadStream::fromPath('/my/file.zip');
		return new \Nucleardog\StreamedResponse\StreamedResponse($fileStream);
	}
}
```

Wherever possible, you should also call the `setEtag` and `setLastModified`
methods on the response before returning so clients can make use of
the `If-Range` header.

You probably also want to set the `Content-Type` header.

### Error Handling

For any issues encountered by this library (i.e., not ones in the underlying
stream library or implementation), an exception extending from
`\Nucleardog\StreamedResponse\Exceptions\StreamedResponseException` will be
thrown. These are generally things like "the header was incorrectly formatted"
or "the user requested a range that doesn't exist in this stream".

The base class adds four methods to simplify handling these errors in most
cases:

* `getStatusCode(): int`: The HTTP status code that applies in this situation.
* `response(): Response`: Generates a Symfony response with the correct status code
  set. This can be returned directly to the user.
* `isUserError(): bool`: Whether this error is caused by the user, generally
  this means "this is something the user can fix by changing their request".
* `isSystemError(): bool`: Whether this error is caused by our program or logic,
  generally this means "this is something the user can't fix".

For example:

```php
<?php
class Controller
{
	public function getFile()
	{
		$fileStream = \Nucleardog\Streams\ReadStream::fromPath('/my/file.zip');
		try
		{
			return new \Nucleardog\StreamedResponse\StreamedResponse($fileStream);
		}
		catch (\Nucleardog\StreamedResponse\Exceptions\StreamedResponseException $ex)
		{
			return $ex->response();
		}
	}
}
```

### Overrides

If, for any reason, you want to override some of the ways the response is
handled there are a few options available to you. These must be called
before you or your framework call `prepare()` on the request.

* `StreamedResponse->setFormatter(formatter)`: Explicitly sets the formatter
  to use when outputting the request.
* `StreamedResponse->setOutputStream(writeable)`: Sets the output stream to
  write the response body to. If not set, defaults to `php://output`.

## Extending

The work of actualling getting a stream out to the browser falls on the
"Formatters". The interface (`src/Contracts/Formatter.php`) requires the
following:

* `handles(request, response): bool`: Returns true if this formatter applies
  to this request and response.
* `prepare(request, response): void`: Make any modifications to the response
  required before we start sending it (e.g., set headers).
* `format(request, response, stream): void`: Output the response body to the
  provided stream.
* `always(request, response): void`: Called on every formatter for every
  response to allow it to do things like advertise its functionality.
  (e.g., adding the `Accept-Ranges` header to advertise that the `Range:`
   header can be used)

Once you implement a formatter, you can add it to the list of formatters that
the StreamedResponse class will consider. A few methods are available:

* `prependFormatter(formatter)`, `appendFormatter(formatter)`: Add the passed
  formatter to the beginning (tried first) or end (tried last) of the list of
  formatters.
* `clearFormatters()`: Clear the current list of formatters and do not restore
  the default list. Calling this before appending or prepending allows you to
  remove all default formatters.
* `setFormatters(formatters)`: Overwrite the list of formatters. No validation
  is done of the array elements. Ensure they are all implementations of the
  Formatter interface.
* `setFallbackFormatter(formatter)`: Set the formatter to use when no formatters
  match.
* `clearFallbackFormatter()`: Clear the fallback formatter, restoring the default.

By default, the response uses the formatters:

* `MultipartRangeFormatter`
* `RangeFormatter`
* `SimpleFormatter` (fallback)

## Tests

Phpunit is included in a separate composer file and must be explicitly
installed:

```
$ cd tools/phpunit/
$ composer install
```

Once installed, the tests can be run from the root package folder with:

```
$ composer test
```

# Legal

Copyright 2024 Adam Pippin <hello@adampippin.ca>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this project except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
