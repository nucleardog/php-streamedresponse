<?php

declare(strict_types=1);

namespace Nucleardog\StreamedResponse\Contracts;

use Symfony\Component\HttpFoundation\Request as SymfonyRequest;
use Nucleardog\Streams\Contracts\Readable;
use Nucleardog\Streams\Contracts\Writeable;
use Nucleardog\StreamedResponse\StreamedResponse;

/**
 * Appropriately configures a response then renders a stream to the response
 * body.
 */
interface Formatter
{
	/**
	 * Apply any unconditional modifications to the response
	 *
	 * This method is called on every formatter for every request to allow them
	 * to do things like set headers to advertise supported functionality.
	 *
	 * This may modify the response.
	 *
	 * @param SymfonyRequest $request
	 * @param StreamedResponse $response
	 * @return void
	 */
	public function always(SymfonyRequest $request, StreamedResponse $response): void;

	/**
	 * Determine if the formatter should handle the given request and response
	 *
	 * Each formatter is asked in turn if it applies to the given transaction.
	 * The first one that answers it does is used to process the response.
	 *
	 * This shall not make any modifications to the request or response.
	 *
	 * @param SymfonyRequest $request
	 * @param StreamedResponse $response
	 * @return bool true if this formatter can handle this request and response
	 */
	public function handles(SymfonyRequest $request, StreamedResponse $response): bool;

	/**
	 * Handle the request and response
	 *
	 * This is called before the response begins sending and is the chance to
	 * modify headers, status code, and other parts of the response. No actual
	 * response body should be generated here.
	 *
	 * This may make modifications to the response.
	 *
	 * @param SymfonyRequest $request
	 * @param StreamedResponse $response
	 * @return void
	 */
	public function prepare(SymfonyRequest $request, StreamedResponse $response): void;

	/**
	 * Output the response body to the provided output stream
	 *
	 * @param SymfonyRequest $request
	 * @param StreamedResponse $response
	 * @param Writeable $output
	 * @return void
	 */
	public function format(SymfonyRequest $request, StreamedResponse $response, Writeable $output): void;
}