<?php

namespace Nucleardog\StreamedResponse\Exceptions;

class IfRangeFormatException extends RangeException
{

	public function getStatusCode(): int
	{
		return 400;
	}

	protected function getDefaultMessage(): string
	{
		return 'The If-Range header was specified but the format or syntax is invalid.';
	}

}