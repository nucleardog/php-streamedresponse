<?php

namespace Nucleardog\StreamedResponse\Exceptions;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

/**
 * A general exception occurred when processing a Range: request.
 *
 * This is the base class for all exceptions thrown by the Range formatters.
 */
class RangeException extends StreamedResponseException
{

	public function getStatusCode(): int
	{
		return 500;
	}

	protected function getDefaultMessage(): string
	{
		return 'Could not process content range';
	}

}