<?php

namespace Nucleardog\StreamedResponse\Exceptions;

class RangeFormatException extends RangeException
{

	public function getStatusCode(): int
	{
		return 400;
	}

	protected function getDefaultMessage(): string
	{
		return 'The Range header was specified but the format or syntax is invalid.';
	}

}