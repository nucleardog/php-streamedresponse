<?php

namespace Nucleardog\StreamedResponse\Exceptions;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;
use Nucleardog\Streams\Contracts\Seekable;

class RangeUnsatisfiableException extends RangeException
{
	private readonly int $length;

	public function __construct(Seekable|int $length, ?string $message = null, ?\Throwable $previous = null)
	{
		$this->length = $length instanceof Seekable ? $length->length() : $length;
		parent::__construct(
			$message ?? $this->getDefaultMessage(),
			$previous,
		);
	}

	public function getStatusCode(): int
	{
		return 416;
	}

	protected function getDefaultMessage(): string
	{
		return 'The Range header was specified but the requested range cannot be satisfied.';
	}

	public function response(): SymfonyResponse
	{
		$response = parent::response();
		$response->headers->set('Content-Range', sprintf('*/%d', $this->length));
		return $response;
	}

}