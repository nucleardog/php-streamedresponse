<?php

namespace Nucleardog\StreamedResponse\Exceptions;

class RangeUnsupportedException extends RangeException
{

	public function getStatusCode(): int
	{
		return 400;
	}

	protected function getDefaultMessage(): string
	{
		return 'A valid Range request was made but we cannot satisfy it due to storage functionality limitations.';
	}

}