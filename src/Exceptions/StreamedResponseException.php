<?php

namespace Nucleardog\StreamedResponse\Exceptions;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

/**
 * Base class for all library exceptions
 */
class StreamedResponseException extends \Exception
{

	public function __construct(?string $message = null, ?\Throwable $previous = null)
	{
		parent::__construct(
			$message ?? $this->getDefaultMessage(),
			0,
			$previous,
		);
	}

	/**
	 * Get the default message to use for this exception if none was provided
	 * when the exception was instantiated.
	 *
	 * @return string
	 */
	protected function getDefaultMessage(): string
	{
		return 'Could not generate response';
	}

	/**
	 * Get the HTTP status code that should apply in the case that this exception
	 * occurs while processing a request and generating a response.
	 *
	 * @return int
	 */
	public function getStatusCode(): int
	{
		return 500;
	}

	/**
	 * Generate a Symfony response with the appropriate status code, headers,
	 * etc for this exception.
	 *
	 * No internal details are included in this and it is safe to send to the
	 * client.
	 *
	 * @return SymfonyResponse
	 */
	public function response(): SymfonyResponse
	{
		$response = new SymfonyResponse();
		$response->setStatusCode($this->getStatusCode());
	}

	/**
	 * Determine whether this exception is classified as "user error" (a mistake
	 * in the request; something the client can correct)
	 *
	 * @return bool
	 */
	public function isUserError(): bool
	{
		return $this->getStatusCode() >= 400 && $this->getStatusCode() < 500;
	}

	/**
	 * Determine whether this exception is classified as "system error" (something
	 * wrong with our code or logic)
	 *
	 * @return bool
	 */
	public function isSystemError(): bool
	{
		return $this->getStatusCode() >= 500 && $this->getStatusCode() < 600;
	}

}