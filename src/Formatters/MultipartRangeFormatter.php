<?php

declare(strict_types=1);

namespace Nucleardog\StreamedResponse\Formatters;

use Symfony\Component\HttpFoundation\Request as SymfonyRequest;
use Nucleardog\Streams\Contracts\Readable;
use Nucleardog\Streams\Contracts\Writeable;
use Nucleardog\Streams\Contracts\Seekable;
use Nucleardog\StreamedResponse\Contracts\Formatter;
use Nucleardog\StreamedResponse\StreamedResponse;

/**
 * Handles requests with a Range: header that specify multiple ranges of content
 * to return.
 *
 * This is split from the "single range" RangeFormatter because the multipart/
 * response format used for multiple ranges is very different. It extends it to
 * reuse some of the common functionality like "evaluating the If-Range header".
 */
class MultipartRangeFormatter extends RangeFormatter
{

	protected array $contentType = [];
	protected array $boundary = [];

	public function handles(SymfonyRequest $request, StreamedResponse $response): bool
	{
		return
			$request->headers->has('Range') &&
			(!is_array($request->headers->get('Range')) || sizeof($request->headers->get('Range')) === 1) &&
			!empty($request->headers->get('Range')) &&
			// I'll probably regret this, but quick check for "single or multipart"
			strpos($request->headers->get('Range'), ',') === true &&
			(!$request->headers->has('If-Range') || $this->ifRangeMatches($request, $response))
			;
	}

	public function prepare(SymfonyRequest $request, StreamedResponse $response): void
	{
		if (!$request->headers->has('Range')) {
			$this->prepareFallback($request, $response);
			return;
		}

		$stream = $response->getStream();
		$ranges = RangeCollection::fromHeader($stream, $request->headers->get('Range'));

		// Our "is this single or multipart" check has failed, but we're already
		// committed.
		if (count($ranges) <= 1) {
			// We'll just ignore the range and return the whole file (valid per
			// the spec), so set nothing.
			$this->prepareFallback($request, $response);
			return;
		}

		// Otherwise we plan to serve a range, set up our headers and stuff.

		// Get the original content type so we can apply it to each part during output
		$contentType = $response->headers->has('Content-Type') ? $response->headers->get('Content-Type') : 'application/octet-stream';
		$this->contentType[spl_object_id($request)] = $contentType;

		// This is a multipart response.
		$this->boundary[spl_object_id($request)] = $boundary = md5((string)time());
		$response->headers->set('Content-Type', sprintf('multipart/byteranges; boundary=%s', $boundary));
		$response->headers->set('Content-Length', (string)$this->calculateResponseLength($ranges, $boundary, $contentType, $stream instanceof Seekable ? (string)$stream->length() : '*'));
		$response->setStatusCode(206);
	}

	private function calculateResponseLength(RangeCollection $ranges, string $boundary, string $contentType, string $streamLength): int
	{
		// TODO: This should really be unified with the actual response generation
		//       before they get out of sync and break stuff.
		$length = 0;
		foreach ($ranges as $range) {
			// '--'
			$length += 2;
			// boundary
			$length += strlen($boundary);
			// \r\n
			$length += 2;
			// Content-Type: whatever \r\n
			$length += 14;
			$length += strlen($contentType);
			$length += 2;
			// Content-Range: 1-100/1234 \r\n
			$length += 15;
			$length += strlen(sprintf('%d-%d/%s', $range->firstByte, $range->lastByte, $streamLength));
			$length += 2;
			// \r\n to end headers
			$length += 2;

			// Actual content
			$length += $range->length();

			// \r\n
			$length += 2;
		}

		// --boundary--
		$length += 2 + strlen($boundary) + 2;

		return $length;
	}

	public function format(SymfonyRequest $request, StreamedResponse $response, Writeable $output): void
	{
		$stream = $response->getStream();

		// If no range or we never prepared this response, then just pass the content through.
		if (!$request->headers->has('Range') ||
			empty($this->boundary[spl_object_id($request)]) ||
			empty($this->contentType[spl_object_id($request)])) {
			$this->formatFallback($request, $response, $output);
			return;
		}

		$ranges = RangeCollection::fromHeader($stream, $request->headers->get('Range'));

		// Our "is this single or multipart" check has failed, but we're already
		// committed.
		if (count($ranges) <= 1) {
			// We messed up, ignore the range and send the content through.
			$this->formatFallback($stream, $output, $request);
			return;
		}

		$contentType = $this->contentType[spl_object_id($request)];
		$boundary = $this->boundary[spl_object_id($request)];
		unset($this->contentType[spl_object_id($request)]);
		unset($this->boundary[spl_object_id($request)]);
		$streamLength = $stream instanceof Seekable ? $stream->length() : '*';

		// Serve the ranges
		foreach ($ranges as $range) {
			$output->write(sprintf("--%s\r\n", $boundary));
			$output->write(sprintf("Content-Type: %s\r\n", $contentType));
			$output->write(sprintf("Content-Range: %d-%d/%s\r\n", $range->firstByte, $range->lastByte, $streamLength));
			$output->write("\r\n");
			$range->render($output);
			$output->write("\r\n");
		}

		$output->write(sprintf('--%s--', $boundary));
	}

}