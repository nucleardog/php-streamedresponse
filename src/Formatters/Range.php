<?php

declare(strict_types=1);

namespace Nucleardog\StreamedResponse\Formatters;
use Nucleardog\Streams\Contracts\Readable;
use Nucleardog\Streams\Contracts\Seekable;
use Nucleardog\Streams\Contracts\Writeable;

/**
 * Represents a range of bytes within a stream and handles extracting that
 * range.
 */
class Range
{

	public function __construct(
		private readonly Readable $stream,
		public readonly int $firstByte,
		public readonly int $lastByte,
	) {
	}

	public function render(Writeable $stream): void
	{
		if ($this->firstByte !== 0) {
			$this->stream->seek($this->firstByte);
		}
		$length = $this->length();
		$this->stream->copy($stream, $length);
	}

	public function length(): int
	{
		return ($this->lastByte - $this->firstByte) + 1;
	}

	public static function fromHeader(Readable $stream, string $header): Range
	{
		$header = explode('=', $header, 2);
		if (sizeof($header) != 2) {
			static::invalidRangeHeader();
		}

		if ($header[0] !== 'bytes') {
			static::invalidRangeHeader();
		}

		$range = explode('-', $header[1], 2);

		if (sizeof($range) != 2 ||
			preg_match('/^[0-9]*$/', $range[0]) === 0 ||
			preg_match('/^[0-9]*$/', $range[1]) === 0 ||
			($range[0] === '' && $range[1] === '')
		) {
			static::invalidRangeHeader();
		}

		if (($range[0] === '' || $range[1] === '') &&
			!($stream instanceof Seekable)) {
			// If the request is for "bytes from the end" or
			// "until the end", we can't satisfy the range
			// without knowing the length of the stream.
			// (Well, we _can_, but we don't know enough to fill
			// the Content-Ranges header before we start serving
			// the request.)
			static::unsupportedRangeHeader();
		}

		// Okay, if all else is good... turn these into ints for our further
		// math and stuff.
		if ($range[0] !== '') {
			$range[0] = (int)$range[0];
		}
		if ($range[1] !== '') {
			$range[1] = (int)$range[1];
		}

		// If no start byte is specified, then we should return $end bytes
		// from the end of the stream.
		if ($range[0] === '') {
			// If the stream has 1000 bytes, then it has indexes 0-999.
			// To return the last 100 bytes, we want to return 900-999 inclusive.
			// So start at $end - $offset + 1, and end at $end.
			return new Range($stream, $stream->length() - $range[1], $stream->length() - 1);
		}
		// If no end byte is specified, then we should stop at the end of the file.
		else if ($range[1] === '') {
			// If the stream has 1000 bytes, then it has indexes 0-999.
			// The last byte, inclusive, is $end - 1
			return new Range($stream, $range[0], $stream->length() - 1);
		}
		// If both values are specified, return those exact bytes.
		else {
			// But first!
			// If the stream is seekable (so we know the length), then validate
			// the ranges.
			if ($stream instanceof Seekable) {
				$length = $stream->length();
				if (
					// Start is negative (shouldn't be possible from regexp above)
					$range[0] < 0 ||
					// Start is past end of file
					$range[0] >= $length ||
					// End is before beginning of file (shouldn't be possible from regexp above)
					$range[1] < 0 ||
					// End is past end of file
					$range[1] >= $length ||
					// Start is after end
					$range[0] > $range[1]
				) {
					static::unsatisfiableRange($length);
				}
			}
			return new Range($stream, $range[0], $range[1]);
		}
	}

	private static function invalidRangeHeader(): void
	{
		throw new \Nucleardog\StreamedResponse\Exceptions\RangeFormatException();
	}

	private static function unsupportedRangeHeader(): void
	{
		throw new \Nucleardog\StreamedResponse\Exceptions\RangeUnsupportedException();
	}

	private static function unsatisfiableRange(int $length): void
	{
		throw new \Nucleardog\StreamedResponse\Exceptions\RangeUnsatisfiableException($length);
	}

}