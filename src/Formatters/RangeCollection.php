<?php

declare(strict_types=1);

namespace Nucleardog\StreamedResponse\Formatters;
use Nucleardog\Streams\Contracts\Readable;
use Nucleardog\Streams\Contracts\Seekable;
use Nucleardog\Streams\Contracts\Writeable;

/**
 * Represents a collection of ranges of bytes within a stream and handles
 * parsing a Range: http header into those ranges.
 */
class RangeCollection implements \Countable, \IteratorAggregate
{

	public function __construct(
		public readonly array $items
	) {
	}

	public function count(): int
	{
		return sizeof($this->items);
	}

	public function getIterator(): \Traversable
	{
		return new \ArrayIterator($this->items);
	}

	public static function fromHeader(Readable $stream, string $header): RangeCollection
	{
		$firstByte = 0;
		$lastByte = $stream instanceof Seekable ? $stream->length() - 1 : null;

		$ranges = explode(',', $header);
		$ranges = array_map(fn($range) => Range::fromHeader($stream, trim($range)), $ranges);

		usort($ranges, function(Range $a, Range $b) {
			if ($a->firstByte < $b->firstByte) {
				return -1;
			} else if ($a->firstByte > $b->firstByte) {
				return 1;
			} else {
				return 0;
			}
		});

		return new RangeCollection($ranges);
	}

}