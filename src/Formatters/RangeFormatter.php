<?php

declare(strict_types=1);

namespace Nucleardog\StreamedResponse\Formatters;

use Symfony\Component\HttpFoundation\Request as SymfonyRequest;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;
use Nucleardog\Streams\Contracts\Readable;
use Nucleardog\Streams\Contracts\Writeable;
use Nucleardog\Streams\Contracts\Seekable;
use Nucleardog\StreamedResponse\Contracts\Formatter;
use Nucleardog\StreamedResponse\StreamedResponse;

/**
 * Handles requests with a Range: header that only specify a single range of
 * content to return.
 *
 * This is split from the "multiple ranges" MultipartRangeFormatter because
 * the multipart/ response format used for multiple ranges is very different.
 */
class RangeFormatter implements Formatter
{

	public function always(SymfonyRequest $request, StreamedResponse $response): void
	{
		// Always set the 'Accept-Ranges' header when this formatter is included
		// in the stack so the browser knows it can make Range requests.
		$response->headers->set('Accept-Ranges', 'bytes');
	}

	public function handles(SymfonyRequest $request, StreamedResponse $response): bool
	{
		return
			$request->headers->has('Range') &&
			(!is_array($request->headers->get('Range')) || sizeof($request->headers->get('Range')) === 1) &&
			!empty($request->headers->get('Range')) &&
			// I'll probably regret this, but quick check for "single or multipart"
			strpos($request->headers->get('Range'), ',') === false &&
			(!$request->headers->has('If-Range') || $this->ifRangeMatches($request, $response))
			;
	}

	public function prepare(SymfonyRequest $request, StreamedResponse $response): void
	{
		if (!$request->headers->has('Range')) {
			$this->prepareFallback($request, $response);
			return;
		}

		$stream = $response->getStream();
		$ranges = RangeCollection::fromHeader($stream, $request->headers->get('Range'));

		// Our "is this single or multipart" check has failed, but we're already
		// committed.
		if (count($ranges) !== 1) {
			// We'll just ignore the range and return the whole file (valid per
			// the spec), so set nothing.
			$this->prepareFallback($request, $response);
			return;
		}

		// Otherwise we plan to serve a range, set the headers and status code.
		$range = array_values($ranges->items)[0];
		$response->headers->set('Content-Range', sprintf(
			'bytes %d-%d/%s',
			$range->firstByte,
			$range->lastByte,
			($stream instanceof Seekable) ? $stream->length() : '*',
		));
		$response->headers->set('Content-Length', (string)$range->length());
		$response->setStatusCode(206);
	}

	protected function prepareFallback(SymfonyRequest $request, StreamedResponse $response): void
	{
		if ($response->getStream() instanceof Seekable) {
			$response->headers->set('Content-Length', (string)$response->getStream()->length());
		}
	}

	public function format(SymfonyRequest $request, StreamedResponse $response, Writeable $output): void
	{
		$stream = $response->getStream();

		// If no range, then just pass the content through.
		if (!$request->headers->has('Range')) {
			$this->formatFallback($request, $response, $output);
			return;
		}

		$ranges = RangeCollection::fromHeader($stream, $request->headers->get('Range'));

		// Our "is this single or multipart" check has failed, but we're already
		// committed.
		if (count($ranges) !== 1) {
			// We messed up, ignore the range and send the content through.
			$this->formatFallback($request, $response, $output);
			return;
		}

		// Serve the range
		$range = array_values($ranges->items)[0];

		if ($stream instanceof Seekable) {
			$stream->seek($range->firstByte);
		} else {
			// Not seekable, read and discard data up to the start
			// This assumes the stream started at the beginning.
			$discard = $range->firstByte;
			for (;;) {
				// Max chunk size of 4mb (4194304) chosen totally arbitrarily.
				$size = min(4194304, $discard);
				$chunk = $stream->read($size);
				$discard -= $size;
				if ($discard === 0) {
					break;
				} else if ($discard < 0) {
					// Guard against an infinite loop if I'm off by one here somewhere
					// ever.
					throw new \RuntimeException('Could not seek in stream');
				}
			}
		}

		$stream->copy($output, $range->length());
	}

	protected function formatFallback(SymfonyRequest $request, StreamedResponse $response, Writeable $output): void
	{
		$response->getStream()->copy($output);
	}

	protected function ifRangeMatches(SymfonyRequest $request, StreamedResponse $response): ?bool
	{
		if (!$request->headers->has('If-Range')) {
			return null;
		}

		$ifRange = $request->headers->get('If-Range');

		if (empty($ifRange)) {
			throw new \Nucleardog\StreamedResponse\Exceptions\IfRangeFormatException();
		}

		// In theory any etag value should be surrounded by quotes. But to be a
		// little more forgiving, we'll just try and parse the value as a date
		// and treat anything else as an etag.
		$ifRangeTimestamp = \DateTime::createFromFormat(\DATE_RFC2822, $ifRange);

		if ($ifRangeTimestamp !== false) {
			$responseLastModifiedTimestamp = $response->getLastModified();
			return !empty($responseLastModifiedTimestamp) && $ifRangeTimestamp == $responseLastModifiedTimestamp;
		} else {
			$etag = $response->getEtag();
			return !empty($etag) && trim($etag, '"') === trim($ifRange, '"');
		}
	}

}