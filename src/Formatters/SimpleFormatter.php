<?php

declare(strict_types=1);

namespace Nucleardog\StreamedResponse\Formatters;

use Symfony\Component\HttpFoundation\Request as SymfonyRequest;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;
use Nucleardog\Streams\Contracts\Readable;
use Nucleardog\Streams\Contracts\Writeable;
use Nucleardog\Streams\Contracts\Seekable;
use Nucleardog\StreamedResponse\Contracts\Formatter;
use Nucleardog\StreamedResponse\StreamedResponse;

class SimpleFormatter implements Formatter
{
	public function always(SymfonyRequest $request, StreamedResponse $response): void
	{
		//
	}

	public function handles(SymfonyRequest $request, StreamedResponse $response): bool
	{
		return true;
	}

	public function prepare(SymfonyRequest $request, StreamedResponse $response): void
	{
		if ($response->getStream() instanceof Seekable) {
			$response->headers->set('Content-Length', (string)$response->getStream()->length());
		}
	}

	public function format(SymfonyRequest $request, StreamedResponse $response, Writeable $output): void
	{
		$response->getStream()->copy($output);
	}

}