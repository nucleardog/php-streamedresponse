<?php

declare(strict_types=1);

namespace Nucleardog\StreamedResponse;

use Symfony\Component\HttpFoundation\Request as SymfonyRequest;
use Symfony\Component\HttpFoundation\StreamedResponse as SymfonyStreamedResponse;
use Nucleardog\Streams\Contracts\Readable;
use Nucleardog\Streams\Contracts\Writeable;
use Nucleardog\Streams\WriteStream;

class StreamedResponse extends SymfonyStreamedResponse
{
	/**
	 * List of formatter implements to use for outputting responses
	 *
	 * Null when uninitialized; will be set to the defaults provided by
	 * static::getDefaultFormatters().
	 *
	 * @var array<Contracts\Formatter>|null
	 */
	protected static ?array $formatters = null;

	/**
	 * If no formatters match the request and response, this formatter will
	 * be used unconditionally.
	 *
	 * If not set, the code is hardcoded to use the "SimpleFormatter" implementation
	 * which simply streams the response.
	 *
	 * @var Contracts\Formatter|null
	 */
	protected static ?Contracts\Formatter $fallback = null;

	/**
	 * Request that this response has been prepare()d for
	 *
	 * Cached so it can be passed in to formatter during the actual response
	 * body generation.
	 *
	 * @var SymfonyRequest
	 */
	protected SymfonyRequest $request;

	/**
	 * Formatter that has been selected to use for this response
	 *
	 * Determined during the prepare() step if it's not explicitly set before
	 * that point.
	 *
	 * @var Contracts\Formatter $formatter
	 */
	public readonly Contracts\Formatter $formatter;

	/**
	 * The output stream to write the response to
	 *
	 * Automatically outputs to php://output if not set before we start
	 * generating the response body.
	 *
	 * @var Writeable
	 */
	private readonly Writeable $output;

	/**
	 * Create a new response to return a stream to the user
	 *
	 * @param Readable $stream
	 */
	public function __construct(
		private readonly Readable $stream,
	) {
		parent::__construct([$this, 'stream']);
	}

	/**
	 * Retrieve the stream this response is wrapping
	 *
	 * @return Readable
	 */
	public function getStream(): Readable
	{
		return $this->stream;
	}

	/**
	 * Explicitly set the formatter that should be used to output this
	 * response.
	 *
	 * @param Contracts\Formatter $formatter
	 * @return static
	 */
	public function setFormatter(Contracts\Formatter $formatter): static
	{
		$this->formatter = $formatter;
		return $this;
	}

	/**
	 * Retrieve the writeable stream the response is being output to
	 *
	 * @return Writeable
	 */
	public function getOutputStream(): Writeable
	{
		if (!isset($this->output)) {
			$this->output = WriteStream::fromPath('php://output');
		}
		return $this->output;
	}

	/**
	 * Set the output stream for the response
	 *
	 * @param Writeable $stream
	 * @return static
	 */
	public function setOutputStream(Writeable $stream): static
	{
		$this->output = $stream;
		return $this;
	}

	/**
	 * Add a response formatter to the beginning of the list (tried first)
	 *
	 * @param Contracts\Formatter $formatter
	 * @return void
	 */
	public static function prependFormatter(Contracts\Formatter $formatter): void
	{
		$formatters = static::getFormatters();
		array_unshift($formatters, $formatter);
		static::setFormatters($formatters);
	}

	/**
	 * Add a response formatter to the end of the list (tried last)
	 *
	 * @param Contracts\Formatter $formatter
	 * @return void
	 */
	public static function appendFormatter(Contracts\Formatter $formatter): void
	{
		$formatters = static::getFormatters();
		$formatters[] = $formatter;
		static::setFormatters($formatters);
	}

	/**
	 * Clear the formatter list and don't restore defaults
	 *
	 * @return void
	 */
	public static function clearFormatters(): void
	{
		static::$formatters = [];
	}

	/**
	 * Set the fallback formatter to be used when no other formatter matches
	 *
	 * @param Contracts\Formatter $formatter
	 * @return void
	 */
	public static function setFallbackFormatter(Contracts\Formatter $formatter): void
	{
		static::$fallback = $formatter;
	}

	/**
	 * Retrieve all registered response formatters
	 *
	 * @return array<Contracts\Formatter>
	 */
	public static function getFormatters(): array
	{
		if (!isset(static::$formatters)) {
			static::$formatters = static::getDefaultFormatters();
		}
		return static::$formatters;
	}

	/**
	 * Overwrite the list of registered response formatters
	 *
	 * @param array<Contracts\Formatter> $formatters
	 * @return void
	 */
	public static function setFormatters(array $formatters): void
	{
		static::$formatters = $formatters;
	}

	/**
	 * Get the default list of formatters used by the library
	 *
	 * @return array<Contracts\Formatter>
	 */
	public static function getDefaultFormatters(): array
	{
		return [
			new Formatters\MultipartRangeFormatter(),
			new Formatters\RangeFormatter(),
		];
	}

	/**
	 * Clear the fallback formatter, restoring the default fallback formatter
	 *
	 * @return void
	 */
	public static function clearFallbackFormatter(): void
	{
		static::$fallback = null;
	}

	/**
	 * Examine all formatters and find the one that can be used for this request
	 * and response pair
	 *
	 * @param SymfonyRequest $request
	 * @param StreamedResponse $response
	 * @return Contracts\Formatter
	 */
	public static function getFormatter(SymfonyRequest $request, StreamedResponse $response): Contracts\Formatter
	{
		// Check each formatter and see if any apply
		foreach (static::getFormatters() as $formatter) {
			if ($formatter->handles($request, $response)) {
				return $formatter;
			}
		}

		// If nothing found, return the fallback formatter.
		if (empty(static::$fallback)) {
			// If no fallback is specified, use the SimpleFormatter that just
			// copies the input stream to the output stream.
			static::$fallback = new Formatters\SimpleFormatter();
		}
		return static::$fallback;
	}

	/**
	 * Make any modifications to the response before starting to send it
	 *
	 * @param SymfonyRequest $request
	 * @return static
	 */
	public function prepare(SymfonyRequest $request): static
	{
		parent::prepare($request);
		$this->request = $request;
		foreach (static::$formatters as $formatter) {
			$formatter->always($request, $this);
		}
		if (!isset($this->formatter)) {
			$this->formatter = $this->getFormatter($request, $this);
		}
		$this->formatter->prepare($request, $this);
		return $this;
	}

	/**
	 * Stream the response to the output
	 *
	 * Internal callback provided to the underlying Symfony StreamedResponse
	 * for it to call when it's time to output the response.
	 *
	 * @return void
	 */
	protected function stream()
	{
		$this->formatter->format($this->request, $this, $this->getOutputStream());
	}
}