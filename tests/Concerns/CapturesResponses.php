<?php

declare(strict_types=1);

namespace Nucleardog\StreamedResponse\Tests\Concerns;
use Nucleardog\StreamedResponse\StreamedResponse;
use Nucleardog\Streams\WriteStream;

trait CapturesResponses
{
	/**
	 * "send" the given response and return the output it generated
	 *
	 * @param StreamedResponse $response
	 * @return string
	 */
	private function captureResponse(StreamedResponse $response): string
	{
		// Open a memory stream for the response to write to
		$stream = fopen('php://memory', 'r+');
		$writeStream = WriteStream::fromStream($stream);
		// Provide it to the response
		$response->setOutputStream($writeStream);

		// "Send" the response, having it generate to the output stream
		$response->send();

		// Rewind the memory stream and fetch the response that was written
		fseek($stream, 0, SEEK_SET);
		$contents = stream_get_contents($stream);
		fclose($stream);

		// Return contents of stream
		return $contents;
	}

}