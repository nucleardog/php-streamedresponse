<?php

declare(strict_types=1);

namespace Nucleardog\StreamedResponse\Tests\Concerns;
use Symfony\Component\HttpFoundation\Request as SymfonyRequest;

trait CreatesRequests
{

	private function createRequest(): SymfonyRequest
	{
		return new SymfonyRequest();
	}

}
