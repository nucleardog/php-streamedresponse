<?php

declare(strict_types=1);

namespace Nucleardog\StreamedResponse\Tests\Concerns;
use Nucleardog\Streams\Contracts\Readable;
use Nucleardog\StreamedResponse\StreamedResponse;
use Nucleardog\StreamedResponse\Contracts\Formatter;
use Symfony\Component\HttpFoundation\Request as SymfonyRequest;

trait CreatesResponses
{

	private abstract function getStream(): Readable;

	/**
	 * Create a request for testing
	 *
	 * @param ?Readable $stream stream to wrap; defaults to calling getStream()
	 * @return StreamedResponse
	 */
	private function createResponse(?Readable $stream = null): StreamedResponse
	{
		return new StreamedResponse($stream ?? $this->getStream());
	}

	/**
 	 * Prepare a response for sending
 	 *
 	 * @param Formatter|array|null $formatter formatter(s) to configure the response class with
 	 * @param SymfonyRequest|null $request request to generate in response to; calls createResponse() if none passed
 	 * @param Readable|StreamedResponse|null response to prepare; created from stream or from default stream if none passed
 	 * @param bool|string $require require a specific formatter to be selected; pass class name or 'true' to select the last formatter in $formatter
 	 * @param Formatter|string|null $fallback set the fallback formatter by instance, class, or null for default
 	 */
	private function prepareResponse(
		Formatter|array|null $formatter = null,
		?SymfonyRequest $request = null,
		Readable|StreamedResponse|null $response = null,
		bool|string $require = true,
		Formatter|string|null $fallback = null,
	): StreamedResponse {
		if ($response instanceof Readable) {
			$response = $this->createResponse($response);
		} else if (is_null($response)) {
			$response = $this->createResponse($this->getStream());
		}

		StreamedResponse::clearFallbackFormatter();
		StreamedResponse::clearFormatters();

		if (isset($formatter)) {
			if (!is_array($formatter)) {
				$formatter = [$formatter];
			}
			foreach ($formatter as $f) {
				StreamedResponse::appendFormatter($f);
			}
		}

		if ($fallback) {
			StreamedResponse::setFallbackFormatter(is_string($fallback) ? new $fallback() : $fallback);
		}

		$request ??= new SymfonyRequest();
		$response->prepare($request);

		if (is_string($require)) {
			$this->assertInstanceOf($require, $response->formatter, 'Desired formatter not selected');
		} else if (!empty($require)) {
			$formatter = is_array($formatter) ? get_class(end($formatter)) : (is_object($fallback) ? get_class($fallback) : $fallback);
			$this->assertInstanceOf($formatter, $response->formatter, 'Desired formatter not selected');
		}

		return $response;
	}

}