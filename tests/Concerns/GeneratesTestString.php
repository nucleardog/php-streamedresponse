<?php

declare(strict_types=1);

namespace Nucleardog\StreamedResponse\Tests\Concerns;

trait GeneratesTestString
{
	private function getTestString(): string
	{
		$str = '';
		for ($i=0; $i<100; $i+=10) {
			$str .= '123-'.str_pad((string)$i, 2, '0', STR_PAD_LEFT).'-890';
		}
		return $str;
	}
}
