<?php

declare(strict_types=1);

namespace Nucleardog\StreamedResponse\Tests;
use Nucleardog\Streams\Contracts\Readable;
use Nucleardog\Streams\ReadStream;
use Nucleardog\Streams\WriteStream;
use Nucleardog\StreamedResponse\Formatters\MultipartRangeFormatter;

/**
 * Tests the MultiPartRangeFormatter's generation of multipart responses
 */
class MultipartRangeFormatterTest extends TestCase
{
	use Concerns\GeneratesTestString;
	use Concerns\CreatesResponses;
	use Concerns\CapturesResponses;
	use Concerns\CreatesRequests;

	private function getStream(): Readable
	{
		return ReadStream::fromString($this->getTestString());
	}

	/**
 	 * Basic multipart response
 	 */
	public function testSimpleMultipart()
	{
		$request = $this->createRequest();
		$request->headers->set('Range', 'bytes=0-49, bytes=50-99');
		$response = $this->prepareResponse(new MultipartRangeFormatter(), request: $request, fallback: new MultipartRangeFormatter());
		$content = $this->captureResponse($response);
		$this->assertSame(true, $response->headers->has('Content-Type'));
		$this->assertSame('multipart/byteranges', explode(';', $response->headers->get('Content-Type'), 2)[0]);
		$this->assertSame(true, $response->headers->has('Content-Length'));
		$this->assertSame((string)strlen($content), $response->headers->get('Content-Length'));

	}

	// I didn't feel like finding or writing a multipart/mime parser today.
	// So I didn't test the rest.

}