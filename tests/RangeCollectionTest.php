<?php

declare(strict_types=1);

namespace Nucleardog\StreamedResponse\Tests;
use Nucleardog\Streams\Contracts\Readable;
use Nucleardog\Streams\ReadStream;
use Nucleardog\Streams\WriteStream;
use Nucleardog\StreamedResponse\Formatters\RangeCollection;
use Nucleardog\StreamedResponse\Formatters\Range;

/**
 * Tests the RangeCollection class's ability to parse the Range header
 */
class RangeCollectionTest extends TestCase
{
	use Concerns\GeneratesTestString;

	private function getStream(): Readable
	{
		return ReadStream::fromString($this->getTestString());
	}

	/**
	 * Simple case of a range explicitly specifying start and end and including
	 * the whole document.
	 */
	public function testSimpleRange()
	{
		$stream = $this->getStream();
		$rc = RangeCollection::fromHeader($stream, 'bytes=0-99');

		$this->assertSame(1, count($rc));
		$range = array_values($rc->items)[0];
		$this->assertSame(0, $range->firstByte);
		$this->assertSame(99, $range->lastByte);
	}

	/**
	 * A range without an end byte, which should use the end of the stream
	 * as the last byte.
	 */
	public function testOpenEndRange()
	{
		$stream = $this->getStream();
		$rc = RangeCollection::fromHeader($stream, 'bytes=0-');

		$this->assertSame(1, count($rc));
		$range = array_values($rc->items)[0];
		$this->assertSame(0, $range->firstByte);
		$this->assertSame(99, $range->lastByte);
	}

	/**
	 * A range starting partway through the stream with no explicit end;
	 * should give us from the specified point to the last byte.
	 */
	public function testOpenEndRangeWithStart()
	{
		$stream = $this->getStream();
		$rc = RangeCollection::fromHeader($stream, 'bytes=50-');

		$this->assertSame(1, count($rc));
		$range = array_values($rc->items)[0];
		$this->assertSame(50, $range->firstByte);
		$this->assertSame(99, $range->lastByte);
	}

	/**
	 * A range with no start byte specified should return the last
	 * N bytes of the file, where N is the "end byte".
	 */
	public function testOpenStartRange()
	{
		$stream = $this->getStream();
		$rc = RangeCollection::fromHeader($stream, 'bytes=-10');

		$this->assertSame(1, count($rc));
		$range = array_values($rc->items)[0];
		$this->assertSame(90, $range->firstByte);
		$this->assertSame(99, $range->lastByte);
	}

	/**
	 * Basic parsing of a header with multiple ranges specified
	 */
	public function testMultipleRanges()
	{
		$stream = $this->getStream();
		$rc = RangeCollection::fromHeader($stream, 'bytes=0-9, bytes=10-19, bytes=20-29');

		$this->assertSame(3, count($rc));

		$range1 = array_values($rc->items)[0];
		$this->assertSame(0, $range1->firstByte);
		$this->assertSame(9, $range1->lastByte);
		$range2 = array_values($rc->items)[1];
		$this->assertSame(10, $range2->firstByte);
		$this->assertSame(19, $range2->lastByte);
		$range3 = array_values($rc->items)[2];
		$this->assertSame(20, $range3->firstByte);
		$this->assertSame(29, $range3->lastByte);
	}

	/**
	 * Ensure that when multiple ranges are specified they are returned in order
	 * of the start byte.
	 *
	 * Just my assumption that most use cases and clients would need the earlier
	 * parts of the file before the later. But mostly added this just to make
	 * the results more deterministic.
	 */
	public function testMultipleRangesSorting()
	{
		$stream = $this->getStream();
		$rc = RangeCollection::fromHeader($stream, 'bytes=10-19, bytes=0-9, bytes=20-29');

		$this->assertSame(3, count($rc));

		$range1 = array_values($rc->items)[0];
		$this->assertSame(0, $range1->firstByte);
		$this->assertSame(9, $range1->lastByte);
		$range2 = array_values($rc->items)[1];
		$this->assertSame(10, $range2->firstByte);
		$this->assertSame(19, $range2->lastByte);
		$range3 = array_values($rc->items)[2];
		$this->assertSame(20, $range3->firstByte);
		$this->assertSame(29, $range3->lastByte);
	}

	/**
	 * Requesting ranges past the end of the stream results in
	 * an unsatisfiable range.
	 */
	public function testUnsatisfiableRangeEndPastLength()
	{
		$stream = $this->getStream();
		$this->expectException(\Nucleardog\StreamedResponse\Exceptions\RangeUnsatisfiableException::class);
		$rc = RangeCollection::fromHeader($stream, 'bytes=0-1000');
	}

	/**
	 * Requesting ranges with a start past the end of the stream results
	 * in an unsatisfiable range.
	 *
	 * This isn't really able to be tested since this shouldn't be able to happen
	 * without triggering either the previous (end of range past end) or next
	 * (end of range before start) conditions.
	 */
	public function testUnsatisfiableRangeStartPastLength()
	{
		$stream = $this->getStream();
		$this->expectException(\Nucleardog\StreamedResponse\Exceptions\RangeUnsatisfiableException::class);
		$rc = RangeCollection::fromHeader($stream, 'bytes=1000-2000');
	}

	/**
	 * Requesting ranges with an end byte before the start byte are unsatisfiable.
	 */
	public function testUnsatisfiableRangeEndBeforeStart()
	{
		$stream = $this->getStream();
		$this->expectException(\Nucleardog\StreamedResponse\Exceptions\RangeUnsatisfiableException::class);
		$rc = RangeCollection::fromHeader($stream, 'bytes=70-50');
	}

	/**
	 * A completely invalid header triggers an improperly formatted header
	 * exception.
	 */
	public function testInvalidRange()
	{
		$stream = $this->getStream();
		$this->expectException(\Nucleardog\StreamedResponse\Exceptions\RangeFormatException::class);
		$rc = RangeCollection::fromHeader($stream, 'asdf');
	}

	/**
	 * Using units we don't support triggers a header format exception.
	 */
	public function testInvalidRangeBadUnits()
	{
		$stream = $this->getStream();
		$this->expectException(\Nucleardog\StreamedResponse\Exceptions\RangeFormatException::class);
		$rc = RangeCollection::fromHeader($stream, 'octets=0-99');
	}

	/**
	 * Passing valid units but no range is an invalidly formatter header.
	 */
	public function testInvalidRangeMissingRange()
	{
		$stream = $this->getStream();
		$this->expectException(\Nucleardog\StreamedResponse\Exceptions\RangeFormatException::class);
		$rc = RangeCollection::fromHeader($stream, 'bytes');
	}

	/**
	 * Passing valid units and an empty range is invalidly formatted header.
	 */
	public function testInvalidRangeEmptyRange()
	{
		$stream = $this->getStream();
		$this->expectException(\Nucleardog\StreamedResponse\Exceptions\RangeFormatException::class);
		$rc = RangeCollection::fromHeader($stream, 'bytes=');
	}

	/**
	 * Not passing start and end is an invalidly formatted header.
	 */
	public function testInvalidRangeNoValues()
	{
		$stream = $this->getStream();
		$this->expectException(\Nucleardog\StreamedResponse\Exceptions\RangeFormatException::class);
		$rc = RangeCollection::fromHeader($stream, 'bytes=-');
	}

	/**
	 * Passing non-numeric values for the range is not properly formatted.
	 */
	public function testInvalidRangeNonNumeric()
	{
		$stream = $this->getStream();
		$this->expectException(\Nucleardog\StreamedResponse\Exceptions\RangeFormatException::class);
		$rc = RangeCollection::fromHeader($stream, 'bytes=a-b');
	}

	/**
	 * Unseekable streams can't automatically determine the last byte and
	 * we don't support serving an open ended range from them.
	 */
	public function testUnsupportedRangeUnseekableNoEnd()
	{
		$stream = $this->getStream();
		// Convert to a non-seekable stream
		$stream = new ReadStream($stream->unwrap());

		$this->expectException(\Nucleardog\StreamedResponse\Exceptions\RangeUnsupportedException::class);
		$rc = RangeCollection::fromHeader($stream, 'bytes=0-');
	}

	/**
	 * Unseekable streams can't automatically determine the last byte and
	 * we don't support serving "the last N bytes" from them.
	 */
	public function testUnsupportedRangeUnseekableFromEnd()
	{
		$stream = $this->getStream();
		// Convert to a non-seekable stream
		$stream = new ReadStream($stream->unwrap());

		$this->expectException(\Nucleardog\StreamedResponse\Exceptions\RangeUnsupportedException::class);
		$rc = RangeCollection::fromHeader($stream, 'bytes=-10');
	}

}