<?php

declare(strict_types=1);

namespace Nucleardog\StreamedResponse\Tests;
use Nucleardog\Streams\Contracts\Readable;
use Nucleardog\Streams\ReadStream;
use Nucleardog\Streams\WriteStream;
use Nucleardog\StreamedResponse\Formatters\RangeFormatter;
use Nucleardog\StreamedResponse\Formatters\SimpleFormatter;

/**
 * Tests for the range formatter that actually generates ranged responses
 * (only for a single specified range)
 */
class RangeFormatterTest extends TestCase
{
	use Concerns\GeneratesTestString;
	use Concerns\CreatesResponses;
	use Concerns\CapturesResponses;
	use Concerns\CreatesRequests;

	private function getStream(): Readable
	{
		return ReadStream::fromString($this->getTestString());
	}

	/**
 	 * When this formatter is in the stack the response should always include
 	 * the `Accept-Ranges` header so the client knows we support range
 	 * requests.
 	 */
	public function testAddsAcceptRangesHeader()
	{
		// Send a request with no Range: header or anything, but the range
		// formatter in the stack. Ensure that the response contains the
		// Accept-Ranges header to signal to the browser that it _can_ make
		// range requests.
		$response = $this->prepareResponse(
			formatter: [
				new RangeFormatter(),
				new SimpleFormatter(),
			],
			require: false,
		);

		// Make sure we didn't actually process this response through the range
		// formatter.
		$this->assertNotInstanceOf(RangeFormatter::class, $response->formatter);

		$this->assertSame(true, $response->headers->has('Accept-Ranges'));
		$this->assertSame('bytes', $response->headers->get('Accept-Ranges'));
	}

	/**
 	 * Simple range fetching the entire stream
 	 */
	public function testSimpleRange()
	{
		$this->testRequestRange(
			rangeStart: 0,
			rangeEnd: 99,
			expectedStart: 0,
			expectedEnd: 99,
			expectedLength: 100,
		);
	}

	/**
 	 * Range with no end specified, should return to the end of the stream
 	 */
	public function testOpenEndRange()
	{
		$this->testRequestRange(
			rangeStart: 0,
			rangeEnd: null,
			expectedStart: 0,
			expectedEnd: 99,
			expectedLength: 100,
		);
	}

	/**
 	 * Range with no end specified but start offset into the middle of the stream,
 	 * should return from middle of stream to end.
 	 */
	public function testOpenEndRangeWithStart()
	{
		$this->testRequestRange(
			rangeStart: 50,
			rangeEnd: null,
			expectedStart: 50,
			expectedEnd: 99,
			expectedLength: 50,
		);
	}

	/**
 	 * Range with no start specified should return the last N bytes
 	 */
	public function testOpenStartRange()
	{
		$this->testRequestRange(
			rangeStart: null,
			rangeEnd: 10,
			expectedStart: 90,
			expectedEnd: 99,
			expectedLength: 10,
		);
	}

	/**
 	 * Partial range at the beginning of the stream
 	 */
	public function testStartRange()
	{
		$this->testRequestRange(
			rangeStart: 0,
			rangeEnd: 9,
			expectedStart: 0,
			expectedEnd: 9,
			expectedLength: 10,
		);
	}

	/**
 	 * Partial range in the middle of the stream
 	 */
	public function testMiddleRange()
	{
		$this->testRequestRange(
			rangeStart: 50,
			rangeEnd: 59,
			expectedStart: 50,
			expectedEnd: 59,
			expectedLength: 10,
		);
	}

	/**
 	 * Partial range at the end of the stream
 	 */
	public function testEndRange()
	{
		$this->testRequestRange(
			rangeStart: 90,
			rangeEnd: 99,
			expectedStart: 90,
			expectedEnd: 99,
			expectedLength: 10,
		);
	}

	/**
 	 * If no range header is specified that the range formatter should not
 	 * be selected.
 	 */
	public function testMissingRange()
	{
		$this->testRequestFallback(null);
	}

	/**
 	 * The If-Range header should only return the ranged response if the condition
 	 * is satisfied.
 	 * Here it is satisfied by matching the response's etag header and should
 	 * return a range.
 	 */
	public function testIfRangeMatchingEtag()
	{
		$etag = 'asdf1234';

		$request = $this->createRequest();
		$request->headers->set('If-Range', sprintf('"%s"', $etag));
		$request->headers->set('Range', 'bytes=0-9');

		$response = $this->createResponse();
		$response->setEtag($etag);
		$response = $this->prepareResponse(new RangeFormatter(), request: $request, response: $response);
		$content = $this->captureResponse($response);

		$this->assertSame(206, $response->getStatusCode(), 'Incorrect status code');
		$this->assertSame(true, $response->headers->has('Content-Range'), 'Missing Content-Range header');
		$this->assertSame('bytes 0-9/100', $response->headers->get('Content-Range'), 'Content-Range header incorrect');
		$this->assertSame(true, $response->headers->has('Content-Length'), 'Missing Content-Length');
		$this->assertSame('10', $response->headers->get('Content-Length'), 'Content-Length header incorrect');
		$this->assertSame(substr($this->getTestString(), 0, 10), $content, 'Wrong body');
	}

	/**
 	 * The If-Range header should only return the ranged response if the condition
 	 * is satisfied.
 	 * Here it is NOT satisfied as the etag is different and it should return
 	 * the whole stream.
 	 */
	public function testIfRangeNonMatchingEtag()
	{
		$etagRequest = 'asdf1234';
		$etagResponse = '4321fdsa';

		$request = $this->createRequest();
		$request->headers->set('If-Range', sprintf('"%s"', $etagRequest));
		$request->headers->set('Range', 'bytes=0-9');

		$response = $this->createResponse();
		$response->setEtag($etagResponse);
		$response = $this->prepareResponse(new RangeFormatter(), request: $request, response: $response, require: false);

		$this->assertNotInstanceOf(RangeFormatter::class, $response->formatter, 'Range formatter still selected');
	}

	/**
 	 * The If-Range header should only return the ranged response if the condition
 	 * is satisfied.
 	 * Here it is satisfied as the last modified time of the response matches
 	 * and only a partial response should be returned.
 	 */
	public function testIfRangeMatchingLastModified()
	{
		$timestamp = new \DateTime();

		$request = $this->createRequest();
		$request->headers->set('If-Range', $timestamp->format('D, d M Y H:i:s').' GMT');
		$request->headers->set('Range', 'bytes=0-9');

		$response = $this->createResponse();
		$response->setLastModified($timestamp);
		$response = $this->prepareResponse(new RangeFormatter(), request: $request, response: $response);
		$content = $this->captureResponse($response);

		$this->assertSame(206, $response->getStatusCode(), 'Incorrect status code');
		$this->assertSame(true, $response->headers->has('Content-Range'), 'Missing Content-Range header');
		$this->assertSame('bytes 0-9/100', $response->headers->get('Content-Range'), 'Content-Range header incorrect');
		$this->assertSame(true, $response->headers->has('Content-Length'), 'Missing Content-Length');
		$this->assertSame('10', $response->headers->get('Content-Length'), 'Content-Length header incorrect');
		$this->assertSame(substr($this->getTestString(), 0, 10), $content, 'Wrong body');
	}

	/**
 	 * The If-Range header should only return the ranged response if the condition
 	 * is satisfied.
 	 * Here it is NOT satisfied as the last modified time of the response is
 	 * different and the whole stream should be returned.
 	 */
	public function testIfRangeNonMatchingLastModified()
	{
		$timestampRequest = new \DateTime();
		$timestampResponse = (new \DateTime())->add(new \DateInterval('P1Y'));

		$request = $this->createRequest();
		$request->headers->set('If-Range', $timestampRequest->format('D, d M Y H:i:s').' GMT');
		$request->headers->set('Range', 'bytes=0-9');

		$response = $this->createResponse();
		$response->setLastModified($timestampResponse);
		$response = $this->prepareResponse(new RangeFormatter(), request: $request, response: $response, require: false);

		$this->assertNotInstanceOf(RangeFormatter::class, $response->formatter, 'Range formatter still selected');
	}

	private function testRequestRange(int $expectedStart, int $expectedEnd, int $expectedLength, ?int $rangeStart, ?int $rangeEnd)
	{
		$request = $this->createRequest();
		$request->headers->set('Range', sprintf(
			'bytes=%s-%s',
			$rangeStart ?? '',
			$rangeEnd ?? '',
		));

		$response = $this->prepareResponse(new RangeFormatter(), request: $request);
		$content = $this->captureResponse($response);

		$this->assertSame(206, $response->getStatusCode(), 'Incorrect status code');
		$this->assertSame(true, $response->headers->has('Content-Range'), 'Missing Content-Range header');
		$this->assertSame(sprintf('bytes %d-%d/100', $expectedStart, $expectedEnd), $response->headers->get('Content-Range'), 'Content-Range header incorrect');
		$this->assertSame(true, $response->headers->has('Content-Length'), 'Missing Content-Length header');
		$this->assertSame((string)$expectedLength, $response->headers->get('Content-Length'), 'Content-Length header incorrect');
		$this->assertSame(substr($this->getTestString(), $expectedStart, $expectedLength), $content, 'Wrong body');
	}

	private function testRequestFallback(?string $range)
	{
		$request = $this->createRequest();
		if (isset($range)) {
			$request->headers->set('Range', $range);
		}

		$response = $this->prepareResponse(new RangeFormatter(), request: $request, fallback: new RangeFormatter());
		$content = $this->captureResponse($response);

		$this->assertSame(200, $response->getStatusCode(), 'Incorrect status code');
		$this->assertSame(false, $response->headers->has('Content-Range'), 'Has Content-Range header');
		$this->assertSame(true, $response->headers->has('Content-Length'), 'Missing Content-Length header');
		$this->assertSame((string)strlen($this->getTestString()), $response->headers->get('Content-Length'), 'Content-Length header incorrect');
		$this->assertSame($this->getTestString(), $content, 'Wrong body');
	}

}