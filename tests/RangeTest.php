<?php

declare(strict_types=1);

namespace Nucleardog\StreamedResponse\Tests;
use Nucleardog\Streams\Contracts\Readable;
use Nucleardog\Streams\ReadStream;
use Nucleardog\Streams\WriteStream;
use Nucleardog\StreamedResponse\Formatters\RangeCollection;
use Nucleardog\StreamedResponse\Formatters\Range;

/**
 * Tests individual ranges being able to correctly pull portions of the
 * underlying stream.
 *
 * Does not test the parsing functionality as that's already well-tested in
 * the RangeCollection test
 */
class RangeTest extends TestCase
{
	use Concerns\GeneratesTestString;

	private function getStream(): Readable
	{
		return ReadStream::fromString($this->getTestString());
	}

	/**
	 * A range at the beginning of the stream
	 */
	public function testRangeRenderStart()
	{
		$testString = $this->getTestString();
		$stream = $this->getStream();
		$range = new Range($stream, 0, 9);
		$this->assertSame(substr($testString, 0, 10), $this->captureRangeOutput($range));
		$this->assertSame(10, $range->length());
	}

	/**
	 * A range in the middle of the stream
	 */
	public function testRangeRenderMiddle()
	{
		$testString = $this->getTestString();
		$stream = $this->getStream();
		$range = new Range($stream, 50, 59);
		$this->assertSame(substr($testString, 50, 10), $this->captureRangeOutput($range));
		$this->assertSame(10, $range->length());
	}

	/**
	 * A range at the end of the stream
	 */
	public function testRangeRenderEnd()
	{
		$testString = $this->getTestString();
		$stream = $this->getStream();
		$range = new Range($stream, 90, 99);
		$this->assertSame(substr($testString, 90, 10), $this->captureRangeOutput($range));
		$this->assertSame(10, $range->length());
	}

	private function captureRangeOutput(Range $range): string
	{
		$stream = fopen('php://memory', 'w+');
		$writeStream = WriteStream::fromStream($stream);
		$range->render($writeStream);
		fseek($stream, 0, SEEK_SET);
		$contents = stream_get_contents($stream);
		fclose($stream);
		return $contents;
	}

	// TODO: Test all the negative cases

}