<?php

declare(strict_types=1);

namespace Nucleardog\StreamedResponse\Tests;
use Nucleardog\Streams\Contracts\Readable;
use Nucleardog\Streams\ReadStream;
use Nucleardog\StreamedResponse\StreamedResponse;
use Nucleardog\StreamedResponse\Contracts\Formatter;
use Nucleardog\StreamedResponse\Formatters\SimpleFormatter;
use Symfony\Component\HttpFoundation\Request as SymfonyRequest;

/**
 * Tests the simple formatter that just copies the response stream out
 * to the browser. This doubles as testing some of our basic test functionality
 * and stuff like "can we create the object at all?".
 */
class SimpleFormatterTest extends TestCase
{
	use Concerns\CreatesResponses;
	use Concerns\CapturesResponses;
	use Concerns\GeneratesTestString;

	private function getStream(): Readable
	{
		return ReadStream::fromString($this->getTestString());
	}

	/**
	 * We can instantiate a StreamedResponse
	 */
	public function testInstantiateResponse()
	{
		$response = $this->createResponse();
		$this->assertInstanceOf(StreamedResponse::class, $response);
	}

	/**
	 * We can instantiate a StreamedResponse and prepare it to be sent
	 */
	public function testPrepareResponse()
	{
		// Don't specify or require any specific formatter, this should return
		// some sort of fallback formatter and we don't care which.
		$response = $this->prepareResponse(require: false);
		$this->assertInstanceOf(StreamedResponse::class, $response);
	}

	/**
	 * The simple formatter returns the entire stream and correctly sets the
	 * status code and content-length
	 */
	public function testStreamResponse()
	{
		// Ensure the fallback formatter just returns the entire stream
		$response = $this->prepareResponse(new SimpleFormatter());
		$content = $this->captureResponse($response);
		$testString = $this->getTestString();
		$this->assertSame(200, $response->getStatusCode(), 'Incorrect status code');
		$this->assertSame(true, $response->headers->has('Content-Length'), 'Missing Content-Length header');
		$this->assertSame((string)strlen($testString), $response->headers->get('Content-Length'), 'Content-Length header incorrect');
		$this->assertSame($testString, $content, 'Wrong body');
	}

}